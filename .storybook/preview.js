import '@storybook/addon-console';
import gLink from "./g-link.vue";
import Vue from 'vue';


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

Vue.component('g-link', gLink)
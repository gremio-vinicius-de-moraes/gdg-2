module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      serif: ["Nunito Sans", "sans-serif"]
    },
    extend: {
      fontSize: {
        desc: ["0.75rem", "19px"],
        descBa: ["1.5em", "1.5em"],
        tit: ["5.063em", "80px"]
      },
      borderWidth: {
        "5/2": "2.54px",
        1: "1px"
      },
      translate: {
        "57/10": "60%"
      },
      colors: {
        blue1: "#0a1c40",
        blue2: "#122a5a",
        blue3: "#4178bf",
        blue4: "#7eaed9",
        title: "#cfd0d6",
        body: "#9da0b2"
      }
    }
  },
  variants: {
    extend: {
      width: ["group-hover"],
      height: ["group-hover"],
      visibility: ["group-hover"],
      alignItems: ["group-hover"],
      verticalAlign: ["hover"],
      padding: ["hover"]
    }
  },
  plugins: []
};

import axios from 'axios';

var files = new Map()
var templates = new Map()

export function getFile(fileName) 
{
    if (files.has(fileName)) {
        return new Promise(resolve => resolve(files.get(fileName)))
    }
    return axios.get(
        `LatexFiles/${fileName}`,
        {responseType: 'arraybuffer'}).then((resp) => {
            console.log(resp)
            files.set(fileName, resp.data)
            return resp.data
        })
}

export function getTemplate(fileName) {
    if (templates.has(fileName)) {
        return new Promise(resolve => resolve(templates.get(fileName)))
    }
    return axios.get(
        `templates/${fileName}.tex`,
        {responseType: 'application/x-tex'}).then((resp) => {
            console.log(resp)
            templates.set(fileName, resp.data)
            return resp.data
        })
}

import {LaTeXEngine} from './latexEngine'
import {getFile} from './getFiles'

function typedArrayToURL(typedArray, mimeType) {
    return URL.createObjectURL(new Blob([typedArray.buffer], {type: mimeType}))
}

var engine = null;
export default {
    async init() {
        const start_init_time = performance.now();
        engine = new LaTeXEngine('PDFLaTeX')
        await engine.loadEngine()
        engine.checkEngineStatus()

        engine.makeMemFSFolder('imgs')

        engine.writeMemFSFile('imgs/logo_sem_fundo.png', await getFile('logo_sem_fundo.png'))
        engine.writeMemFSFile('imgs/marquinhos_mascara.jpeg', await getFile('marquinhos_mascara.jpeg'))
        engine.writeMemFSFile('docsStyle.sty', await getFile('docsStyle.sty'))

        await this.loadRun()
        const past_time = performance.now() - start_init_time;
        console.log(`Initialized in ${past_time}ms` )
    },
    async loadRun() {
        // Load Run para que ele caregue os dados básicos com antecedencia.

        engine.writeMemFSFile('loadRun.tex', await getFile('loadRun.tex'))
        engine.setEngineMainFile('loadRun.tex')
        return engine.compileLaTeX().then((res) => {
            console.log("Load Run Results", res)
            res.pdf = null;

            return res
        }).catch((e) => {
            console.error(e)
        });
    },

    closeWorker() {
        console.log("Destroing the Worker")
        engine && engine.closeWorker();
        return
    },

    async compile(latexCode) {
        
        const start_compile_time = performance.now()
        engine.checkEngineStatus()

        // Load main file
        engine.writeMemFSFile('main.tex', latexCode)
        engine.setEngineMainFile('main.tex')

        engine.checkEngineStatus()
        
        let result = await engine.compileLaTeX();
        
        // Check if one of the errors ask for a rerun
        if (result.errors.some((item) => (item.message.includes("Rerun"))))
        {
            console.log("Re-running to correct references...")
            result = await engine.compileLaTeX();
        }
        console.log(result.errors)
        console.log(result)

        // Gemerate URL for the pdf
        let pdfUrl = ''
        if (result.status == 0 )
        {
            pdfUrl = typedArrayToURL(result.pdf, 'application/pdf')
        }
        console.log(pdfUrl)

        const past_time = performance.now() - start_compile_time;
        console.log(`Compiled finished in ${past_time} ms`)
        
        return {
            errors: result.errors,
            log: result.log,
            pdfRaw: result.pdf,
            status: result.status,
            past_time: past_time,
            urlPdf: pdfUrl
        }

    }
}
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaTeXEngine = exports.CompileResult = exports.EngineStatus = exports.LatexParser = exports.DEFAULT_ENGINE_VERSION = exports.EngineVersions = void 0;
exports.EngineVersions = {
    XeLaTeX: true,
    PDFLaTeX: true,
};
exports.DEFAULT_ENGINE_VERSION = 'XeLaTeX';
// LATEX LOG PARSER
// Define some constants
var LOG_WRAP_LIMIT = 79;
var LATEX_WARNING_REGEX = /^LaTeX Warning: (.*)$/;
var HBOX_WARNING_REGEX = /^(Over|Under)full \\(v|h)box/;
var PACKAGE_WARNING_REGEX = /^(Package \b.+\b Warning:.*)$/;
// This is used to parse the line number from common latex warnings
var LINES_REGEX = /lines? ([0-9]+)/;
// This is used to parse the package name from the package warnings
var PACKAGE_REGEX = /^Package (\b.+\b) Warning/;
var LogText = /** @class */ (function () {
    function LogText(text) {
        this.text = text.replace(/(\r\n)|\r/g, '\n');
        // Join any lines which look like they have wrapped.
        var wrappedLines = this.text.split('\n');
        this.lines = [wrappedLines[0]];
        var i = 1;
        while (i < wrappedLines.length) {
            // If the previous line is as long as the wrap limit then
            // append this line to it.
            // Some lines end with ... when LaTeX knows it's hit the limit
            // These shouldn't be wrapped.
            if (wrappedLines[i - 1].length === LOG_WRAP_LIMIT &&
                wrappedLines[i - 1].slice(-3) !== '...') {
                this.lines[this.lines.length - 1] += wrappedLines[i];
            }
            else {
                this.lines.push(wrappedLines[i]);
            }
            i++;
        }
        this.row = 0;
    }
    LogText.prototype.nextLine = function () {
        this.row++;
        if (this.row >= this.lines.length) {
            return false;
        }
        else {
            return this.lines[this.row];
        }
    };
    LogText.prototype.rewindLine = function () {
        this.row--;
    };
    LogText.prototype.linesUpToNextWhitespaceLine = function () {
        return this.linesUpToNextMatchingLine(/^ *$/);
    };
    LogText.prototype.linesUpToNextMatchingLine = function (match) {
        var lines = [];
        var nextLine = this.nextLine();
        if (nextLine !== false) {
            lines.push(nextLine);
        }
        while (nextLine !== false && !nextLine.match(match)) {
            nextLine = this.nextLine();
            if (nextLine !== false) {
                lines.push(nextLine);
            }
        }
        return lines;
    };
    return LogText;
}());
var state = {
    NORMAL: 0,
    ERROR: 1,
};
var LatexParser = /** @class */ (function () {
    function LatexParser(text) {
        this.log = new LogText(text);
        this.state = state.NORMAL;
        this.fileBaseNames = [/compiles/, /\/usr\/local/];
        this.ignoreDuplicates = false;
        this.data = [];
        this.fileStack = [];
        this.currentFileList = this.rootFileList = [];
        this.openParens = 0;
    }
    LatexParser.prototype.parse = function () {
        while ((this.currentLine = this.log.nextLine()) !== false) {
            if (this.state === state.NORMAL) {
                if (this.currentLineIsError()) {
                    this.state = state.ERROR;
                    this.currentError = {
                        line: null,
                        file: this.currentFilePath,
                        level: 'error',
                        message: this.currentLine.slice(2),
                        content: '',
                        raw: this.currentLine + '\n',
                    };
                }
                else if (this.currentLineIsRunawayArgument()) {
                    this.parseRunawayArgumentError();
                }
                else if (this.currentLineIsWarning()) {
                    this.parseSingleWarningLine(LATEX_WARNING_REGEX);
                }
                else if (this.currentLineIsHboxWarning()) {
                    this.parseHboxLine();
                }
                else if (this.currentLineIsPackageWarning()) {
                    this.parseMultipleWarningLine();
                }
                else {
                    this.parseParensForFilenames();
                }
            }
            if (this.state === state.ERROR) {
                this.currentError.content += this.log
                    .linesUpToNextMatchingLine(/^l\.[0-9]+/)
                    .join('\n');
                this.currentError.content += '\n';
                this.currentError.content += this.log.linesUpToNextWhitespaceLine().join('\n');
                this.currentError.content += '\n';
                this.currentError.content += this.log.linesUpToNextWhitespaceLine().join('\n');
                this.currentError.raw += this.currentError.content;
                var lineNo = this.currentError.raw.match(/l\.([0-9]+)/);
                if (lineNo) {
                    this.currentError.line = parseInt(lineNo[1], 10);
                }
                this.data.push(this.currentError);
                this.state = state.NORMAL;
            }
        }
        return this.postProcess(this.data);
    };
    LatexParser.prototype.currentLineIsError = function () {
        return this.currentLine[0] === '!';
    };
    LatexParser.prototype.currentLineIsRunawayArgument = function () {
        return this.currentLine.match(/^Runaway argument/);
    };
    LatexParser.prototype.currentLineIsWarning = function () {
        return !!this.currentLine.match(LATEX_WARNING_REGEX);
    };
    LatexParser.prototype.currentLineIsPackageWarning = function () {
        return !!this.currentLine.match(PACKAGE_WARNING_REGEX);
    };
    LatexParser.prototype.currentLineIsHboxWarning = function () {
        return !!this.currentLine.match(HBOX_WARNING_REGEX);
    };
    LatexParser.prototype.parseRunawayArgumentError = function () {
        this.currentError = {
            line: null,
            file: this.currentFilePath,
            level: 'error',
            message: this.currentLine,
            content: '',
            raw: this.currentLine + '\n',
        };
        this.currentError.content += this.log.linesUpToNextWhitespaceLine().join('\n');
        this.currentError.content += '\n';
        this.currentError.content += this.log.linesUpToNextWhitespaceLine().join('\n');
        this.currentError.raw += this.currentError.content;
        var lineNo = this.currentError.raw.match(/l\.([0-9]+)/);
        if (lineNo) {
            this.currentError.line = parseInt(lineNo[1], 10);
        }
        return this.data.push(this.currentError);
    };
    LatexParser.prototype.parseSingleWarningLine = function (prefix_regex) {
        var warningMatch = this.currentLine.match(prefix_regex);
        if (!warningMatch) {
            return;
        }
        var warning = warningMatch[1];
        var lineMatch = warning.match(LINES_REGEX);
        var line = lineMatch ? parseInt(lineMatch[1], 10) : null;
        this.data.push({
            line: line,
            file: this.currentFilePath,
            level: 'warning',
            message: warning,
            raw: warning,
        });
    };
    LatexParser.prototype.parseMultipleWarningLine = function () {
        // Some package warnings are multiple lines, let's parse the first line
        var warningMatch = this.currentLine.match(PACKAGE_WARNING_REGEX);
        if (!warningMatch) {
            return;
        }
        // Something strange happened, return early
        var warning_lines = [warningMatch[1]];
        var lineMatch = this.currentLine.match(LINES_REGEX);
        var line = lineMatch ? parseInt(lineMatch[1], 10) : null;
        var packageMatch = this.currentLine.match(PACKAGE_REGEX);
        var packageName = packageMatch[1];
        // Regex to get rid of the unnecesary (packagename) prefix in most multi-line warnings
        var prefixRegex = new RegExp('(?:\\(' + packageName + '\\))*[\\s]*(.*)', 'i');
        // After every warning message there's a blank line, let's use it
        while (!!(this.currentLine = this.log.nextLine())) {
            lineMatch = this.currentLine.match(LINES_REGEX);
            line = lineMatch ? parseInt(lineMatch[1], 10) : line;
            warningMatch = this.currentLine.match(prefixRegex);
            warning_lines.push(warningMatch[1]);
        }
        var raw_message = warning_lines.join(' ');
        this.data.push({
            line: line,
            file: this.currentFilePath,
            level: 'warning',
            message: raw_message,
            raw: raw_message,
        });
    };
    LatexParser.prototype.parseHboxLine = function () {
        var lineMatch = this.currentLine.match(LINES_REGEX);
        var line = lineMatch ? parseInt(lineMatch[1], 10) : null;
        this.data.push({
            line: line,
            file: this.currentFilePath,
            level: 'typesetting',
            message: this.currentLine,
            raw: this.currentLine,
        });
    };
    // Check if we're entering or leaving a new file in this line
    LatexParser.prototype.parseParensForFilenames = function () {
        var pos = this.currentLine.search(/\(|\)/);
        if (pos !== -1) {
            var token = this.currentLine[pos];
            this.currentLine = this.currentLine.slice(pos + 1);
            if (token === '(') {
                var filePath = this.consumeFilePath();
                if (filePath) {
                    this.currentFilePath = filePath;
                    var newFile = {
                        path: filePath,
                        files: [],
                    };
                    this.fileStack.push(newFile);
                    this.currentFileList.push(newFile);
                    this.currentFileList = newFile.files;
                }
                else {
                    this.openParens++;
                }
            }
            else if (token === ')') {
                if (this.openParens > 0) {
                    this.openParens--;
                }
                else {
                    if (this.fileStack.length > 1) {
                        this.fileStack.pop();
                        var previousFile = this.fileStack[this.fileStack.length - 1];
                        this.currentFilePath = previousFile.path;
                        this.currentFileList = previousFile.files;
                    }
                }
            }
            // else {
            // 		 Something has gone wrong but all we can do now is ignore it :(
            // }
            // Process the rest of the line
            this.parseParensForFilenames();
        }
    };
    LatexParser.prototype.consumeFilePath = function () {
        // Our heuristic for detecting file names are rather crude
        // A file may not contain a space, or ) in it
        // To be a file path it must have at least one /
        if (!this.currentLine.match(/^\/?([^ \)]+\/)+/)) {
            return false;
        }
        var endOfFilePath = this.currentLine.search(RegExp(' |\\)'));
        var path;
        if (endOfFilePath === -1) {
            path = this.currentLine;
            this.currentLine = '';
        }
        else {
            path = this.currentLine.slice(0, endOfFilePath);
            this.currentLine = this.currentLine.slice(endOfFilePath);
        }
        return path;
    };
    LatexParser.prototype.postProcess = function (data) {
        var all = [];
        var errors = [];
        var warnings = [];
        var typesetting = [];
        var hashes = [];
        var hashEntry = function (entry) { return entry.raw; };
        var i = 0;
        while (i < data.length) {
            if (this.ignoreDuplicates && hashes.indexOf(hashEntry(data[i])) > -1) {
                i++;
                continue;
            }
            if (data[i].level === 'error') {
                errors.push(data[i]);
            }
            else if (data[i].level === 'typesetting') {
                typesetting.push(data[i]);
            }
            else if (data[i].level === 'warning') {
                warnings.push(data[i]);
            }
            all.push(data[i]);
            hashes.push(hashEntry(data[i]));
            i++;
        }
        return {
            errors: errors,
            warnings: warnings,
            typesetting: typesetting,
            all: all,
            files: this.rootFileList,
        };
    };
    return LatexParser;
}());
exports.LatexParser = LatexParser;
// latexEngine
/********************************************************************************
 * Copyright (C) 2019 Elliott Wen.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
var EngineStatus;
(function (EngineStatus) {
    EngineStatus[EngineStatus["Init"] = 1] = "Init";
    EngineStatus[EngineStatus["Ready"] = 2] = "Ready";
    EngineStatus[EngineStatus["Busy"] = 3] = "Busy";
    EngineStatus[EngineStatus["Error"] = 4] = "Error";
})(EngineStatus = exports.EngineStatus || (exports.EngineStatus = {}));
var XELATEX_ENGINE_PATH = 'bin/swiftlatex.js';
var PDFLATEX_ENGINE_PATH = 'bin/swiftlatexpdftex.js';
var CompileResult = /** @class */ (function () {
    function CompileResult() {
        this.pdf = undefined;
        this.status = -254;
        this.log = 'No log';
        this.errors = [];
    }
    return CompileResult;
}());
exports.CompileResult = CompileResult;
var LaTeXEngine = /** @class */ (function () {
    function LaTeXEngine(engineType) {
        if (engineType === void 0) { engineType = 'XeLaTeX'; }
        this.enginePath = XELATEX_ENGINE_PATH;
        this.engineType = 'XeLaTeX';
        this.latexWorker = undefined;
        this.latexWorkerStatus = EngineStatus.Init;
        this.engineType = engineType;
        if (engineType !== 'XeLaTeX') {
            this.enginePath = PDFLATEX_ENGINE_PATH;
        }
    }
    LaTeXEngine.prototype.loadEngine = function () {
        return __awaiter(this, void 0, Promise, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.latexWorker !== undefined) {
                            throw new Error('Other instance is running, abort()');
                        }
                        this.latexWorkerStatus = EngineStatus.Init;
                        return [4 /*yield*/, new Promise(function (resolve, reject) {
                                _this.latexWorker = new Worker(_this.enginePath);
                                _this.latexWorker.onmessage = function (ev) {
                                    var data = ev.data;
                                    var cmd = data.result;
                                    if (cmd === 'ok') {
                                        _this.latexWorkerStatus = EngineStatus.Ready;
                                        resolve();
                                    }
                                    else {
                                        _this.latexWorkerStatus = EngineStatus.Error;
                                        reject();
                                    }
                                };
                            })];
                    case 1:
                        _a.sent();
                        this.latexWorker.onmessage = function (_) { };
                        this.latexWorker.onerror = function (_) { };
                        return [2 /*return*/];
                }
            });
        });
    };
    LaTeXEngine.prototype.isReady = function () {
        return this.latexWorkerStatus === EngineStatus.Ready;
    };
    LaTeXEngine.prototype.checkEngineStatus = function () {
        if (!this.isReady()) {
            throw Error('Engine is still spinning or not ready yet!');
        }
    };
    LaTeXEngine.prototype.parseLog = function (log) {
        var parser = new LatexParser(log);
        var result = parser.parse();
        // console.log(log);
        // console.log(result);
        var converterError = [];
        for (var t = 0; t < result.errors.length; t++) {
            var element = result.errors[t];
            if (element.line === null || element.line === undefined) {
                element.line = 0;
            }
            var tmp = {
                startLineNumber: element.line,
                endLineNumber: element.line,
                startColumn: 0,
                endColumn: 1024,
                message: element.message + ' in line ' + element.line,
                severity: 4,
                source: element.file ? element.file : 'Entry',
            };
            converterError.push(tmp);
        }
        for (var t = 0; t < result.warnings.length; t++) {
            var element = result.warnings[t];
            if (element.line === null || element.line === undefined) {
                element.line = 0;
            }
            var tmp = {
                startLineNumber: element.line,
                endLineNumber: element.line,
                startColumn: 0,
                endColumn: 1024,
                message: element.message + ' on line ' + element.line,
                severity: 1,
                source: element.file ? element.file : 'Entry',
            };
            converterError.push(tmp);
        }
        return converterError;
    };
    LaTeXEngine.prototype.compileLaTeX = function (desiredDVI) {
        if (desiredDVI === void 0) { desiredDVI = false; }
        return __awaiter(this, void 0, Promise, function () {
            var res;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.checkEngineStatus();
                        this.latexWorkerStatus = EngineStatus.Busy;
                        return [4 /*yield*/, new Promise(function (resolve, _) {
                                _this.latexWorker.onmessage = function (ev) {
                                    var data = ev.data;
                                    var cmd = data.cmd;
                                    if (cmd !== 'compile') {
                                        return;
                                    }
                                    var result = data.result;
                                    var log = data.log;
                                    var status = data.status;
                                    _this.latexWorkerStatus = EngineStatus.Ready;
                                    var nice_report = new CompileResult();
                                    nice_report.status = status;
                                    nice_report.log = log;
                                    nice_report.errors = _this.parseLog(log);
                                    if (result === 'ok') {
                                        var pdf = new Uint8Array(data.pdf);
                                        nice_report.pdf = pdf;
                                    }
                                    else {
                                        if (nice_report.errors.length === 0) {
                                            var dummyAnnotation = {
                                                startLineNumber: 1,
                                                endLineNumber: 1,
                                                startColumn: 0,
                                                endColumn: 1024,
                                                message: "Unexpected error happened, please check the detailed log. Status " + status + ", Engine: " + _this.engineType,
                                                severity: 4,
                                                source: 'SwiftLaTeX',
                                            };
                                            nice_report.errors = [dummyAnnotation];
                                        }
                                    }
                                    resolve(nice_report);
                                };
                                _this.latexWorker.postMessage({ cmd: 'compilelatex', dvi: desiredDVI });
                            })];
                    case 1:
                        res = _a.sent();
                        this.latexWorker.onmessage = function (_) { };
                        return [2 /*return*/, res];
                }
            });
        });
    };
    /* Internal Use */
    LaTeXEngine.prototype.compileFormat = function () {
        return __awaiter(this, void 0, Promise, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.checkEngineStatus();
                        this.latexWorkerStatus = EngineStatus.Busy;
                        return [4 /*yield*/, new Promise(function (resolve, reject) {
                                _this.latexWorker.onmessage = function (ev) {
                                    var data = ev.data;
                                    var cmd = data.cmd;
                                    if (cmd !== 'compile') {
                                        return;
                                    }
                                    var result = data.result;
                                    var log = data.log;
                                    // const status: number = data['status'] as number;
                                    _this.latexWorkerStatus = EngineStatus.Ready;
                                    if (result === 'ok') {
                                        var formatArray = data.pdf; /* PDF for result */
                                        var formatBlob = new Blob([formatArray], {
                                            type: 'application/octet-stream',
                                        });
                                        var formatURL_1 = URL.createObjectURL(formatBlob);
                                        setTimeout(function () {
                                            URL.revokeObjectURL(formatURL_1);
                                        }, 30000);
                                        console.log('Download format file via ' + formatURL_1);
                                        resolve();
                                    }
                                    else {
                                        reject(log);
                                    }
                                };
                                _this.latexWorker.postMessage({ cmd: 'compileformat' });
                            })];
                    case 1:
                        _a.sent();
                        this.latexWorker.onmessage = function (_) { };
                        return [2 /*return*/];
                }
            });
        });
    };
    LaTeXEngine.prototype.setEngineMainFile = function (filename) {
        this.checkEngineStatus();
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ cmd: 'setmainfile', url: filename });
        }
    };
    LaTeXEngine.prototype.writeMemFSFile = function (filename, srccode) {
        this.checkEngineStatus();
        if (srccode === undefined) {
            return;
        }
        if (srccode instanceof ArrayBuffer) {
            srccode = new Uint8Array(srccode);
        }
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ cmd: 'writefile', url: filename, src: srccode });
        }
    };
    LaTeXEngine.prototype.makeMemFSFolder = function (folder) {
        this.checkEngineStatus();
        if (this.latexWorker !== undefined) {
            if (folder === '' || folder === '/') {
                return;
            }
            this.latexWorker.postMessage({ cmd: 'mkdir', url: folder });
        }
    };
    LaTeXEngine.prototype.flushCache = function () {
        this.checkEngineStatus();
        if (this.latexWorker !== undefined) {
            // console.warn('Flushing');
            this.latexWorker.postMessage({ cmd: 'flushcache' });
        }
    };
    LaTeXEngine.prototype.closeWorker = function () {
        if (this.latexWorker !== undefined) {
            this.latexWorker.postMessage({ cmd: 'grace' });
            this.latexWorker = undefined;
        }
    };
    return LaTeXEngine;
}());
exports.LaTeXEngine = LaTeXEngine;

import TextBodyInput from "./TextBodyInput.vue"

export default {
    title: "Atoms/Text Body Input",
    component: TextBodyInput
}

const Template = (args, {argTypes}) => ({
    components: {TextBodyInput},
    template: '<TextBodyInput v-bind="$props">',
    props: Object.keys(argTypes),
})

export const Default = Template.bind({})
Default.args = {
    label: "Hello",
}
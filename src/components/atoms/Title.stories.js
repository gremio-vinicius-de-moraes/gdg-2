import Title from "./Title.vue";

export default {
  title: "Atoms/Title",
  component: Title
};

const Template = (args, { argTypes }) => ({
  components: { Title },
  template: `<Title v-bind="$props" />`,
  props: Object.keys(argTypes)
});

export const Ata = Template.bind({});
Ata.args = { text: "Ata" };

import Description from "./Description.vue";

export default {
  title: "Atoms/Description",
  component: Description
};

const Template = (args, { argTypes }) => ({
  components: { Description },
  template: `<Description v-bind="$props" />`,
  props: Object.keys(argTypes)
});

export const Ata = Template.bind({});
Ata.args = {
  text:
    "A ata é um registro de todas os acontecimentos e decições de uma reunião de um grupo de pessoas. Ela deve ser redigida pelo secretário durante ou após a reunião e deve ser aprovada em próxima reunião."
};

import SerialInput from './SerialInput'

export default {
    title: "Atoms/Serial Input",
    component: SerialInput
}

const Template = (args, {argTypes}) => ({
    components: {SerialInput},
    props: Object.keys(argTypes),
    template: '<SerialInput v-bind="$props" />'
})

export const Default = Template.bind({})
Default.args = {
    value: "07/2021"
}
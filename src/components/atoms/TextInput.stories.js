import TextInput  from "./TextInput.vue";

export default {
    title: "Atoms/Text Input",
    component: TextInput
}

const Template = (args, {argTypes}) => ({
    components: {TextInput},
    template: `<TextInput :label="label" />`,
    props: Object.keys(argTypes),
})

export const TituloInput = Template.bind({});
TituloInput.args = { label: "Titulo" };
import docOption from './docOption.vue'

export default {
    title: "Molecules/Document Option",
    component: docOption,
}

export const Ata = () => ({
    components: {docOption},
    template: '<doc-option name="Ata" description="A ata é um registro de todas os acontecimentos e decições de uma reunião de um grupo de pessoas. Ela deve ser redigida pelo secretário durante ou após a reunião e deve ser aprovada em próxima reunião." />'
})
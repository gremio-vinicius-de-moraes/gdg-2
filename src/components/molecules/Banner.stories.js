import Banner from "./Banner.vue";

export default {
  title: "Molecules/Document Banner",
  component: Banner
};

const Template = (args, { argTypes }) => ({
  components: { Banner },
  template: `<Banner v-bind="$props" />`,
  props: Object.keys(argTypes)
});

export const Ata = Template.bind({});
Ata.args = {
  title: "Ata",
  description:
    "A ata é um registro de todas os acontecimentos e decições de uma reunião de um grupo de pessoas. Ela deve ser redigida pelo secretário durante ou após a reunião e deve ser aprovada em próxima reunião."
};

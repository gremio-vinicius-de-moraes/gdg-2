# Gerador de Documentos do Grêmio 2

Este é o GDG 2.
Criado para gerar documentos para o grêmio usando templates em latex

## Para desenvolver 
### Instalar uma cópia do projeto
1. `git clone https://gitlab.com/gremio-vinicius-de-moraes/gdg-2.git`
2. `yarn` para instalar dependencias
3. `yarn develop`

### Enviar contribuições
1. Branch do `master`
2. Programe
3. Pull o novo branch
4. Envie um merge request para `master`

## Pastas importantes
### static/bin
Engines do latex

### static/templates
Templates usados na gerração dos documentos

### static/LatexFiles
Arquivos usados para compilar o Latex